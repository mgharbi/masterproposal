\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Related Work}{3}
\contentsline {subsection}{\numberline {2.1}Motion magnification}{3}
\contentsline {subsection}{\numberline {2.2}Video alignment and time warping}{4}
\contentsline {subsection}{\numberline {2.3}Image registration and optical flow}{5}
\contentsline {section}{\numberline {3}Proposed approach}{6}
\contentsline {subsection}{\numberline {3.1}Model}{6}
\contentsline {subsection}{\numberline {3.2}Optimizing for the spatio-temporal correspondences}{7}
\contentsline {subsection}{\numberline {3.3}Multi-scale Processing}{9}
\contentsline {subsection}{\numberline {3.4}Amplification and rendering}{9}
\contentsline {subsection}{\numberline {3.5}Expected pitfalls}{10}
\contentsline {section}{\numberline {4}Evaluation}{10}
\contentsline {subsection}{\numberline {4.1}Qualitative and quantitative measures}{10}
\contentsline {subsection}{\numberline {4.2}Limitations}{11}
\contentsline {section}{\numberline {5}Modalities}{11}
\contentsline {subsection}{\numberline {5.1}Resources and data}{11}
\contentsline {subsection}{\numberline {5.2}Deliverables}{12}
\contentsline {subsection}{\numberline {5.3}Timeline}{12}
\contentsline {section}{\numberline {6}Conclusion}{12}
