Though the application we discuss in this proposal is essentially new, our work draws
from several current ideas from the literature.
The spatio-temporal correspondence algorithm we have in mind borrows from the
state-of-the-art in optical flow research, and is also to be put in perspective with
work on video alignment. Since we plan to work in both the spatial and temporal
domain, we also discuss relevant work from the time warping literature. But more
fundamentally, at the core of our motivations lies the idea of revealing
charactersitics of a motion that are not immediately visible, which extrapolates
from the recent body of work on motion magnification.

\subsection{Motion magnification}
    The Motion Magnification algorithm has been designed as a tool to amplify
    minute changes in the world, much like a `motion microscope', revealing
    temporal variations that are difficult or impossible to see with the naked
    eye. Our work's aim is to take this idea a step further and magnify motion
    components where two instances of a same action differ.

    The seminal work~\cite{Liu:2005:MM:1073204.1073223} adopted a Lagrangian
    approach: the authors begin with estimating the motion via tracking, then magnify
    it by amplify the translation from a reference position.    They proceed by
    first registering all the frames in the input video to the first frame.
    Then, they estimate the motion by tracking a sparse set of features,
    decompose the video into layers of independent motions, and render a new
    video where the motion of a chosen layer is amplified by a fixed factor
    compared to the reference frame.

    Motion estimation is a computationally expensive and sometimes fragile
    component, and errors and noise at this stage of the pipeline adversely
    affects the magnified result. More recent work on video magnification
    bypasses motion estimation altogether by adopting an Eulerian
    approach~\cite{journals/tog/WuRSGDF12}. The trade-off is that the resulting
    method is restricted to the magnification of motions with a small
    amplification factor, before artifacts appear. Work
    from~\cite{Wadhwa:2013:PVM:2461912.2461966} extends the Eulerian formulation
    with a phase-based approach that better handles noise and supports larger
    amplification factors. Whereas motion magnification deals with the
    amplification of small motions in a single input video, we seek to compare
    small differences in larger scale motions. For this reason, we cannot rely on
    a similar Eulerian approach. We need to establish correspondences between
    the videos. Correspondences are indeed essential in determining what
    comparable elements are in both videos, and therefore what will get amplified. 

    Another branch of work, whose goals are directly opposite to motion
    magnification, tries to remove (or de-animate) large-scale motion in order
    to better visualize other interesting movements in
    videos~\cite{journals/tog/BaiAAR12}, such as the trajectory of the ball in a
    Roulette game, once the movement of the wheel has been subtracted, or for
    the production of cinemagraphs. A similar idea,
    motion-denoising~\cite{conf/cvpr/RubinsteinLSDF11} seek to remove the high
    frequency small-scale motion to keep only the large scale variations in a
    video stream. The work has been used to remove the short-term flickers in
    timelapse videos of growing plants for example. The novelty in our case is
    that we amplify or attenuate parts of the motion in a video, not only based
    on a scale or frequency criterion, but in relation to the corresponding
    motion in a second video.

\subsection{Video alignment and time warping}
    A vast literature aims at tackling the problem of temporal alignment of
    similar actions in videos \cite{conf/eccv/SinghCMB08},
    \cite{1238449},\cite{6331541}. Most methods rely on the tracking of a sparse
    set of features in the videos to be matched, then perform a
    Dynamic-Time-Warping-like processing to align time series of
    multidimensional features. Our approach differs in that we want a dense
    method that jointly estimates spatial and temporal offsets, using aggregated
    information from both inputs. In these methods, correspondences commonly
    have to be specified manually for each pair of tracked features which we
    hope to avoid. They are also limited to integer frame offsets whereas we aim
    for sub-frame accuracy. 

    Canonical Time Warping~\cite{conf/nips/ZhouT09} tries to align human actions
    and reformulates the Dynamic Time Warping algorithm to embed advantages from
    Canonical Correlation Analysis such as a feature selection mechanism. The
    Generalized Time Warping algorithm~\cite{conf/cvpr/ZhouT12a} takes it
    further and aligns time series from different modalities: it can align a
    video input with motion capture or accelerometer data that have different
    dimensionality. In the context of temporally aligning video data, these
    methods proceed by first tracking a set of discrete features throughout the
    videos. Corresponding features in the two videos are indicated by the user,
    and the algorithm optimizes a non-parametric time-warping function so as to
    best match the tracked features. 

    A different but related problem, is that of spatio-temporally aligning
    videos of the exact same scene captured by two distinct static cameras. The
    cameras can differ in their positions, field of view, framerate\dots
    State-of-the-art approaches use a parametric model of the spatial disparity
    either as a homography or fundamental matrix. Likewise, the temporal warping
    function is assumed to be affine. In contrast we want to build a locally
    varying, non-parametric and data-driven correspondence map.
    In~\cite{Caspi02spatio-temporalalignment}
    and~\cite{caspi_feature_seq2seq_IJCV2006}, the parameters of the
    spatio-temporal correspondence are estimated either using the trajectories
    of a set of tracked features, or directly from the brightness information of
    the input videos as we plan to do.~\cite{journals/tmm/DiegoSL13} extends the
    approach to deal with moving cameras, whose motion follow similar
    trajectories.~\cite{conf/eccv/UkrainitzI06} tackle the spatio-temporal
    alignment of sequences shot at different times and places or of videos of
    different people wearing different clothes by working with space-time
    correlations of the video intensity information. Though we ultimately would
    like our work to be applicable in the same conditions, this is beyond the
    scope of the proposed thesis work.

    \cite{conf/cvpr/ShechtmanI05} compares motions while avoiding
    the explicitly computation of motion fields. Here again, direct
    space-time correlation of the intensity information is used to find
    parts of a video that are similar to a desired motion template. The output
    of their method is a map indicating where and when the motion in the
    video is most
    similar to the query template. This gives a coarse estimation of where
    similar motions occur, but no further attempt is made to quantify or
    amplify differences when motions are dissimilar. Furthermore,
    variations in timing or large geometric deformations of the 
    template are not handled.

    Our method differs from the existing literature because we estimate a
    spatio-temporally varying space-time disparity map between the two input
    videos. This allows us in particular to automatically obtain different time
    offsets for different parts of a video where previous work either estimate a
    global time transformation or require the user to indicate parts to track in
    the videos. This non-uniform time offset matters in the comparison because
    it captures the subtle differences we are trying to shed light on. The price
    to pay is the added complexity of our model.


\subsection{Image registration and optical flow}
    In the process of comparing motions from two input videos, we first
    establish correspondences between the videos. This problem can be seen as an
    extension of the image registration problem where one tries to align two 2D
    input images. We formulate our video alignment procedure as a variational
    energy minimization problem. This formulation is adapted from 2D dense
    optical flow estimation methods. 
    
    During the past few years, much work built on the original variational
    approach~\cite{Horn81determiningoptical}. Among the most significant
    improvements is the use of robust cost functions in the optimization
    procedure~\cite{Black199675}. Aditionally, iterative refinements of the flow
    via warping have been proposed on top of a multiscale decomposition of the
    problem to cope with large displacements, as well as non linearity in the
    robust cost functions~\cite{Brox04highaccuracy}. More advanced
    regularization schemes have been proposed such as structure motion adaptive
    regularization~\cite{Bruhn05lucas/kanademeets},~\cite{conf/iccv/WedelCPB09}.
    Sun et al.~\cite{Sun10secretsof} analyze a number of important practices
    that can make the simple original variational optical flow formulation
    highly competitive with more modern and complex methods. We try to
    incorporate these advances in our formulation when possible.

    Notable approaches like Particle video~\cite{Teller} tried to find a
    compromise between sparse feature tracking and fully dense optical flow. The
    goal of Particle Video is to obtain long-range trajectories where tracking
    usually fails, that are temporally coherent, which optical flow
    cannot offer, at least with the two frames approach.

    Some attempts have been made to improve optical flow using temporal
    information beyond the usual two frames problem~\cite{Black91robustdynamic}\cite{conf/eccv/Nagel90}, and to estimate a
    temporally coherent optical flow~\cite{Weickert2001}. These attempts most
    often add a temporal regularization term, but never extend the flow to have
    a time component.

    Some rare generalizations of optical flow to 3D volume data in the medical
    imaging field have been touched upon~\cite{zhang2008use}. They track
    temporal changes in 3D image data, but to the best of our knowlege, no work has
    used a three-dimensional extension of optical flow for 2D motion processing
    applications.
