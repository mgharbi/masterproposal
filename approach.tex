    In this section we discuss a tentative plan of action to achieve our goals. 
    Good correspondences are a central aspect for the comparison of motions. We
    begin with the description of our ongoing work on the spatio-temporal
    alignment algorithm.

    \subsection{Model}
        Given two grayscale input videos
        $A,B:\Omega\subset\mathbb{R}^3\rightarrow\mathbb{R}$, our goal is to
        estimate a spatio-temporal warping field $\textbf{u} =
        (u(x,y,t),v(x,y,t),w(x,y,t))^T$ at every voxels $\textbf{x} =
        (x,y,t)^T\in\Omega$ such that voxel $\textbf{x}$ in video $A$
        corresponds to $\textbf{x}+\textbf{u}$ in video $B$.
        Fig.~\ref{fig:STcorrespondences} summarizes the notations.

        \begin{figure}[ht]
            \centering
            \psfrag{VideoA}{Video $A$}
            \psfrag{VideoB}{Video $B$}
            \psfrag{A}{{$A(x,y,t)$}}
            \psfrag{B}{{$B(x+u,y+v,t+w)$}}
            \includegraphics[width=5in]{images/STcorrespondences}
            \caption{
                We represent the input videos $A$ and $B$ as spatio-temporal 3D
                volumes. Our algorithm finds a spatio-temporal correspondence
                field $(u,v,w)$ that best aligns the input videos $A$,$B$. The
                optimization allows a spatially varying time offset. As a
                consequence, a frame in the first video will be mapped to a 2D
                manifold in the second video.
            }
            \label{fig:STcorrespondences}
        \end{figure}

        We assume that the input videos depicts similar motions, and are shot in
        the same illumination conditions and against the same background. 
        Under these assumptions, we rely on a spatio-temporal version of the brightness
        constancy assumption \cite{Horn81determiningoptical}. 
        We formalize our goal as a variational energy minimization
        problem. More precisely, we want to find the warping field \textbf{u} so as to
        minimize the following functional: 
        
        \begin{equation}
            E(\textbf{u}) =
            E_D(\textbf{u}) + E_S(\textbf{u}) 
            \label{eq:energy_total}
        \end{equation}

        The first term accounts for fidelity to the data and quantify deviation
        to the brightness constancy
        model. The algorithm effectively tries to match the brightness
        values between $A$ and $B$:
        \begin{equation}
            E_D(\textbf{u}) =
            \int_\Omega 
                \rho_D( |A(\textbf{x})-B(\textbf{x}+\textbf{u})|^2 )
            d\textbf{x}
            \label{eq:energy_data}
        \end{equation}

        The second term is a space-time smoothness prior that enforces
        spatial smoothness and temporal coherence of the warping field. It
        regularizes the underconstrained problem that would consist in minimizing $E_D$ only.
        The smoothness term is responsible for the coupling between neighboring
        voxels and allows the filling-in of areas where the sole data term
        does not provide sufficient information:

        \begin{equation}
            E_S(\textbf{u}) =
            \int_\Omega 
                \lambda_S\{\rho_S( |\nabla u|^2 ) +
                         \rho_S( |\nabla v|^2 )\} +
                \lambda_T\rho_T( |\nabla w|^2 )
            d\textbf{x}
            \label{eq:energy_smoothness}
        \end{equation}

        The gradient operator should be read in the spatio-temporal sense:
        $\nabla=(\frac{\partial}{\partial x},\frac{\partial}{\partial
        y},\frac{\partial}{\partial t})^T$.
        In order to lessen the influence of outliers during the optimization, we chose to
        use a robust cost function instead of the usual quadratic cost \cite{Black199675}.
        This allows discontinuities in the warping field and makes the resulting
        warping field piecewise smooth. We use for the same cost for $\rho_D,\rho_S$ and $\rho_T$
        and pick the Charbonnier cost function \cite{conf/icip/CharbonnierBAB94}:
        \begin{equation}
            \rho(x^2) = \sqrt{x^2+\epsilon^2}
            \label{eq:energy_cost}
        \end{equation}

        We fix $\epsilon=0.001$. Its role is to make the cost function 
        differentiable and convex, which will be helpful for the numerical
        optimization later on.

        The regularization parameters $\lambda_S$, $\lambda_T$ are used to balance the
        smoothness and the data terms. We allow independent
        regularization of the spatial $(u,v)$ and temporal ($w$) components of
        the warping field to account for the difference in units and resolution
        between space and time. The spatial resolution being typically much higher than
        the temporal resolution.

        \subsection{Optimizing for the spatio-temporal correspondences}

        Minimizing the total energy \eqref{eq:energy_total} is a non-linear
        problem because of the terms $B(\textbf{x}+\textbf{u})$ and $\rho$. In
        order to minimize the total energy, we adopt an iterative scheme. We use
        a continuation method that relies on two nested fixed-point (FP)
        iterations. The outer FP iteration enables us to relax the non-linearity
        of the $B(\textbf{x}+\textbf{u})$ term, while the inner FP iteration
        deals with the non-linearity in $\rho$. Within these iterations, the
        reduced problem consists in solving a large and sparse linear system. 

        We let $(B_x,B_y,B_t) = \nabla B$ and define $C(\textbf{x})=
        A(\textbf{x}) - B(\textbf{x}+\textbf{u})$.
        Vanishing of the first order variations in $\textbf{u}$ in equation \eqref{eq:energy_total}
        gives rise to the following Euler Lagrange equations:

        \begin{equation}
            \left\{
                \begin{array}{lll}
                     0 &= \rho_D'(|C|^2)B_x C &+\ \lambda_S \nabla\cdot(\rho_S'(|\nabla u|^2)\nabla u) \\
                     0 &= \rho_D'(|C|^2)B_y C &+\ \lambda_S \nabla\cdot(\rho_S'(|\nabla v|^2)\nabla v) \\
                     0 &= \rho_D'(|C|^2)B_t C &+\ \lambda_T \nabla\cdot(\rho_T'(|\nabla w|^2)\nabla w) \\
                \end{array}
            \right.
            \label{eq:energy_euler_lagrange}
        \end{equation}

        In what follows, we detail the derivation of our iterative optimization
        scheme only for the first equation of the system, the other two being
        analogous. As in \cite{Brox04highaccuracy}, we first use a fixed-point
        iteration to find $\textbf{u}$. Let $\textbf{u}^{(k)}$ be the estimate
        of $\textbf{u}$ a the $k$-th iteration. For the quantities
        $B_x,B_y,B_t,C$ the superscript index $k$ in parentheses indicate that
        the associated quantity is evaluated in $\textbf{u}^{(k)}$. We
        initialize $\textbf{u}^{(0)}=0$ and computes $\textbf{u}^{(k+1)}$ from
        $\textbf{u}^{(k)}$ using the following semi-implicit scheme:

        \begin{equation}
                \begin{array}{ll}
                    0 &= \rho_D'(|C^{(k+1)}|^2)B_x^{(k)} C^{(k+1)} 
                      +\ \lambda_S \nabla\cdot(\rho_S'(|\nabla u^{(k+1)}|^2)\nabla u^{(k+1)}) \\
                \end{array}
            \label{eq:energy_implicit}
        \end{equation}

        % To overcome the non-linearity in $C$, we begin with a fixed-point iteration on $\textbf{u}$. In practice
        % implemented by warping and resample cubic.

        We decompose $\textbf{u}^{(k+1)} = \textbf{u}^{(k)}+\delta\textbf{u}^{(k)}$
        as the sum of the previous field plus an increment.
        Within this iteration, we perform a first order Taylor expansion of the
        data term on the increment $\delta\textbf{u}^{(k)}$:
        \begin{equation}
            \begin{array}{ll}
                C^{(k+1)} &= A(\textbf{x}) - B(\textbf{x}+ \textbf{u}^{(k+1)})\\
                          &= A(\textbf{x}) - B(\textbf{x}+ \textbf{u}^{(k)}+ \delta\textbf{u}^{(k)})\\
                          &= A(\textbf{x}) - B(\textbf{x}+ \textbf{u}^{(k)}) - \nabla B \cdot \delta\textbf{u}^{(k)}\\
                          &= C^{(k)} - \nabla B^{(k)} \cdot \delta\textbf{u}^{(k)}\\
            \end{array}
            \label{eq:linearized_C}
        \end{equation}

        Pluging this decomposition back in
        Eq.~\eqref{eq:energy_implicit}, we obtain :
        \begin{equation}
                \begin{array}{ll}
                    0 &= \rho_D'^{(k)}B_x^{(k)}( C^{(k)} - B_x^{(k)}\delta
                    u^{(k)} - B_y^{(k)}\delta v^{(k)}- B_t^{(k)}\delta w^{(k)}) 
                       +\ \lambda_S \nabla\cdot(\rho_{S,u}'^{(k)}\nabla (u^{(k)}+\delta u^{(k)}) ) \\
                \end{array}
            \label{eq:energy_brightness_linearized}
        \end{equation}

        Where $\rho_i'^{(k)} = \rho_i'(|C^{(k)} - \nabla
        B^{(k)}\cdot\delta\textbf{u}^{(k)}|^2)$ for $i=D,S$.
        This equation is still non-linear because of the non-linearity of the
        cost function $\rho$. To cope with this non-linearity, we perform a second fixed-point
        iteration, this time on $\delta\textbf{u}^{(k)}$. We initialize
        $\delta\textbf{u}^{(k,0)} = 0$, and compute $\delta\textbf{u}^{(k,l+1)}$
        from $\delta\textbf{u}^{(k,l)}$ using the following update rule:

        \begin{equation}
            \begin{array}{lll}
                0 &= \rho_D'^{(k,l)}B_x^{(k)}\{ C^{(k)} - B_x^{(k)}\delta
                    u^{(k,l+1)} \\
                    &-\ B_y^{(k,l+1)}\delta v^{(k,l+1)}- B_t^{(k)}\delta
                    w^{(k,l+1)}\} \\
                  &+\ \lambda_S \nabla\cdot(\rho_{S,u}'^{(k,l)}\nabla (u^{(k)}+\delta
                    u^{(k,l+1)}) ) \\
            \end{array}
            \label{eq:energy_linearized}
        \end{equation}

        Where $\rho_i'^{(k,l)} = \rho_i'(|C^{(k)} - \nabla
        B^{(k)}\cdot\delta\textbf{u}^{(k,l)}|^2)$. That is we use the current estimate
        $\delta\textbf{u}^{(k,l)}$ to update the terms that involve $\rho$, then
        use these updated terms to get the new value of
        $\delta\textbf{u}^{(k,l+1)}$. The derivation for the other two equations
        of Eq.~\eqref{eq:energy_euler_lagrange} is analogous.

        For a fixed iteration $(k,l)$, we discretize
        Eq.~\eqref{eq:energy_linearized} using standard finite differences. The
        remaining problem now ammounts to solving a large
        and sparse linear system $M^{(k,l)}\delta\textbf{u}^{(k,l+1)} =
        b^{(k,l)}$ which we solve with a standard preconditionned conjuguate
        gradient scheme, using an ${\text{ILU}(0)}$ preconditionner
        \cite{Saad:2003:IMS:829576}. We typically use $2$ inner FP iterations on
        $l$, $10$ outer FP iterations on $k$ and $20$ iterations of the linear
        solver.

        % NOTE: this params give a good balance between accuracy and speed

        As suggested in \cite{Sun10secretsof}, we post-process the warping field
        $u^{(k)}$ at the end of each $k$-iteration using a
        color-and-occlusion-weighted median filtering \cite{1187.49020}.

        To evaluate terms of the form $B(\textbf{x}+\textbf{u}^{(k)})$, we warp
        $B$ towards $A$ using the current $k$-th estimate of $u$ via cubic
        interpolation. As shown in \cite{Brox04highaccuracy} this warping step
        allows us to estimate greater and more precise displacements. 
        We compute the spatial derivatives $B_x,B_y$ with a
        5-points derivative filter, and the temporal derivative $B_t$ with a 2-points
        filter (the temporal resolution being typically lower).

        We plan to investigate different regularization term, and in particular
        decompose the smoothness term to allow independent
        discontinuities in $(x,y,t)$ and weigh differently the spatial and temporal
        components of the warping field along in each dimension.
        We will experiment with different cost functions.
        including non-convex cost functions by using a
        Graduated Non-Convexity  approach \cite{Blake:1987:VR:30394}. 

    \subsection{Multi-scale Processing}
        Since we rely on a linearization of the data term $C$ in the
        optimization procedure described previously, the warping field estimated
        must have a small magnitude in order to stay within a regime where the
        linear approximation is valid. If the correspondence we estimate
        has too large a spatio-temporal extent, one can expect the optimization
        procedure to be trapped in a local minimum due to the high frequency
        content of the videos, and miss the global optimum \cite{Black199675}
        \cite{Memin:2002:HES:598426.598464} \cite{710828}

        To cope with this problem we will employ a multi-scale approach: we
        first build a spatio-temporal Gaussian pyramid for the two input videos
        \cite{Caspi02spatio-temporalalignment} by Gaussian filtering the video
        volumes then downsampling. A common scale factor between two
        consecutive pyramid levels is $0.8$. We start by solving the problem at the
        coarsest scale then upsample and scale the computed warping field to the
        next pyramid level. We warp video $B$ towards video $A$ using this
        up-sampled field and proceed to compute $\textbf{u}$ as described in the
        previous subsection.

    \subsection{Amplification and rendering}

    In this section we sketch a simple approach to amplify the spatio-temporal
    differences between two candidate motions. After computing the
    correspondence field $(u,v,w)$ between $A$ and $B$ as described in
    Section~\ref{sec:approach}, we render a new video $A'$ that emphasizes the
    spatial or temporal characteristics that make $A$ different from $B$. We
    plan to explore several amplication strategies and work with various motion
    characteristics such as timing, ordering, velocity, motion amplitude\ldots

    A simple approach would be to `push' each video in opposite ways along the
    direction defined by the correspondence field with a constant amplification
    factor. Indeed, if the pixels from $B$ must be displaced by $\textbf{u}$ to
    match video $A$, moving them by $-\alpha\textbf{u}$ will emphasize
    differences between the video streams. The new video $A'$ can be rendered
    from the pixels of both inputs via forward warping
    \cite{Wolberg:1994:DIW:528718} \cite{Baker:2011:DEM:1960926.1960983}. We
    might need to use a layered approach \cite{Liu:2005:MM:1073204.1073223}
    \cite{Baker98alayered} in order to deal with occlusion and disoclusions.

    More complex motion processing can be performed using the field $(u,v,w)$
    and other cues. Long range correspondences within the videos would help in
    obtaining meaningful time series on which an array of signal processing
    tools can be applied. We envision to be able to compare speeds, perform
    spatiotemporal filtering to choose frequency band, filter by amplitude\ldots
    And maybe use some previous motion magnification work on the aligned videos.

    \subsection{Expected pitfalls}

    Since we will ultimately extrapolate on the existing motion, we will
    undoubtedly face sampling issues and holes will appear in the newly rendered
    video. We might use off-the-shelf methods such as space-time completion of
    videos \cite{Wexler:2007:SCV:1263141.1263424} to fix these glitches and
    improve our rendered output.
    The video volume representation will probably be fine for the matching phase
    but it might show limitations in the reconstruction phase where we might
    need to acquire long-term point trajectories to in order to perform
    meaningful temporal signal processing. 
