% $Id$
%
% LaTeX format for theses at MIT
% Based on "Specifications for Thesis Preparation" 

% `vi' and `upcase' options by Krishna Sethuraman - krishna@athena.mit.edu
% Margins and heading types by Peter Nuth  - nuth@ai.mit.edu
% Title and abstract page by Stephen Gildea - gildea@erl.mit.edu
% Look in this directory for example file mitthesis.doc
% Also for propcover.tex - Boilerplate for PHD proposal.

% To use this style - say something like:
%  for dull, boring thesis format:
%       \documentclass[12pt]{mitthesis}
%       \pagestyle{plain}
% OR for fast drafts: 
%       \documentclass[11pt,singlespace,draft]{mitthesis}
%       \pagestyle{drafthead}
% OR for Tech Reports:
%       \documentclass[12pt,twoside]{mitthesis} 
%       \pagestyle{headings}
% OR
%  some other combination...
%
%%%% New options:
% 
% Option `twoside':
%   Good for producing Tech Reports.
%   The default is single-sided printing, which is what M.I.T. wants on the
%   thesis document itself.
%
% Option `singlespace':
%   Good for drafts.
%   Double-spaced theses are the default.
%   That is what M.I.T. asks for in the formal specifications.
%
%       Note that MIT does not REQUIRE all theses to be double-spaced anymore.
%       Someone in the library system said that it's OK to be single-spaced.
%       (Regardless of what the specs. say...)
%   To get singlespacing in an area - Use  the 'singlespace' environment. 
%
% Option `draft':
%   Puts `overfull' boxes at the end of lines that are too long. 
%
% Pagestyle `drafthead':
%   Puts the date and the label ``*DRAFT*'' in the footer.
%
%%%%%%%%%%
%
%%%% Parameters to initialize for boilerplate page:
%
%       \title{Mixed Circular Cylindrical Shells}
%       \author{J. Casey Salas}
%       \prevdegrees{B.S., University of California (1978) \\
%                    S.M., Massachusetts Institute of Technology (1981)}
%       \department{Department of Electrical Engineering and Computer Science}
%       \degree{Doctor of Philosophy}
%% If the thesis is for two degrees simultaneously, list them both
%% separated by \and like this:
%       \degree{Doctor of Philosophy \and Master of Science}
%       \degreemonth{February}
%       \degreeyear{1987}
%       \thesisdate{December 10, 1986}
%% If the thesis is copyright by the Institute, leave this line out and
%% the standard copyright line will be used instead.
%       \copyrightnotice{J. Casey Salas, 1986}
%% If there is more than one supervisor, use the \supervisor command
%% once for each.
%       \supervisor{John D. Galli}{Director, Sound Instrument Laboratory}
%% This is the department committee chairman, not the thesis committee chairman
%       \chairman{Arthur C. Smith}
%                {Chairman, Departmental Committee on Graduate Students}
%% Make the titlepage based on the above information.  If you need
%% something special and can't use the standard form, you can specify
%% the exact text of the titlepage yourself.  Put it in a titlepage
%% environment and leave blank lines where you want vertical space.
%% The spaces will be adjusted to fill the entire page.  The dotted
%% lines for the signatures are made with the \signature command.
%
%% The abstractpage environment sets up everything on the page except
%% the text itself.  The title and other header material are put at the
%% top of the page, and the supervisors are listed at the bottom.  A
%% new page is begun both before and after.  Of course, an abstract may
%% be more than one page itself.  If you need more control over the
%% format of the page, you can use the abstract environment, which puts
%% the word "Abstract" at the beginning and single spaces its text.
%
%       \begin{abstractpage}
%           Abstract goes here.
%       \end{abstractpage}
%
%%%%%%%% Newer additions 
%
% documentclass options - 
% vi            For MIT course VI or VIII thesis - will copyright the thesis to
%               you while giving MIT permission to copy and distribute it.
% upcase        Will put much of the cover page in uppercase, as per the
%               example on page 17 of the *Specifications for Thesis
%               Preparation*, (revised 1989)
% Also added ``All Rights Reserved'' to default copyright notice.
%
%%%%%%%%%%%
% 
% Documentclass options (vi and upcase) and changes to copyright notice
%       Copyright (c) 1990, by Krishna Sethuraman.
%
% Pagestyle and header generation
%       Copyright (c) 1987, 1988 by Peter Nuth
%
% Original version
%        Copyright (c) 1987 by Stephen Gildea
% Permission to copy all or part of this work is granted, provided
% that the copies are not made or distributed for resale, and that
% the copyright notice and this notice are retained.
% 
% THIS WORK IS PROVIDED ON AN "AS IS" BASIS.  THE AUTHOR PROVIDES NO
% WARRANTY WHATSOEVER, EITHER EXPRESS OR IMPLIED, REGARDING THE WORK,
% INCLUDING WARRANTIES WITH RESPECT TO ITS MERCHANTABILITY OR FITNESS
% FOR ANY PARTICULAR PURPOSE.
%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mitproposal}[1999/10/20]

\def\mystretch{1.5}             % Double spacing hack
\DeclareOption{doublespace}{}   % This is default
                                % So we do not read this style twice
\DeclareOption{singlespace}{            % If he explicitly wants single spacing
    \typeout{Single spaced}
    \def\mystretch{1}}  

%% `vi' and `upcase' document style options.  Krishna Sethuraman (1990)
\newcount\vithesis
\DeclareOption{vi}{\typeout{Course VI/VIII thesis style.}\advance\vithesis by1}
\vithesis=0

\DeclareOption{upcase}{\typeout{Uppercase cover page.}
        \gdef\choosecase#1{\uppercase\expandafter{#1}}}
\def\choosecase#1{#1}

%% leftblank option by Kevin Fu
\newif\if@leftblank \@leftblankfalse

\DeclareOption{leftblank}{\typeout{Intentionally Leaving Pages Blank}
\@leftblanktrue}

%  Thesis looks much like report
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions
\LoadClass{article}

% If the user wants single spacing, set baselinestretch=1.

\usepackage{setspace}

%%%%%%%%%%%%%%%%%MISC USEFUL PACKAGES%%%%%%%%%%%%%%%%%%%%%%%%
%% Package for making an indexed todo list
%% They are all removed in the final version, but show up in draft
% \fxnote{}: notes
% \fxwarn{}: warnings
% \fxerror{}: errors
% \fixme{}: critical errors, throw error in final mode
\usepackage[inline,nomargin]{fixme} % for fixing things later

% We should always include graphics, even in draft mode.
% Eliminate the "final" option to have empty boxes.
%  and faster processing in draft mode.
\usepackage[final]{graphicx}

% Various useful math macros and symbols
\usepackage{amsmath}

% Package for including code and making it pretty
\usepackage{lgrind}

% Does line-breaks intelligently on URLs
\usepackage{url}

% make the URLs in the bibliography look less bad
\usepackage{ragged2e}

% Note - doublespace.sty has some float-related troubles in
% combination with graphics or color, and is not officially compliant
% with 2e.  setspace is a replacement which is 2e-compliant.

% Read the doublespace style that we got from Rochester:
%\input setdoublespace.sty              

\def\baselinestretch{\mystretch}        % Double spacing hack

%%%%%%%  Set up margins and formatting params %%%

% Margins.
%  Note we want 1in top margin assuming no header line, so push header
%       into 1in margin.
%  Draft mode brings the header back down.

\setlength{\oddsidemargin}{0.25in}      % 1.25in left margin 
\setlength{\evensidemargin}{0.25in}     % 1.25in left margin (even pages)
\setlength{\topmargin}{0.0in}           % 1in top margin
\setlength{\textwidth}{6.0in}           % 6.0in text - 1.25in rt margin
\setlength{\textheight}{9in}            % Body ht for 1in margins
\addtolength{\topmargin}{-\headheight}  % No header, so compensate
\addtolength{\topmargin}{-\headsep}     % for header height and separation

% The next two macros compensate page style for headers and footers
% We only need them in page styles that USE headers and footers.
    % If we have a header, it must be 1in from top of page.
\def\pulldownheader{                    % Shift header down 1in from top
    \addtolength{\topmargin}{\headheight}       
    \addtolength{\topmargin}{\headsep}  
    \addtolength{\textheight}{-\headheight}
    \addtolength{\textheight}{-\headsep}
}
    % If we have a footer, put it 1in up from bottom
\def\pullupfooter{                              % Shift footer up
    \addtolength{\textheight}{-\footskip}
%    \addtolength{\textheight}{-\footheight}  %footheight doesn't
%                                               exist in 2e
}

%%%%%%%  End of margins and formatting params %%%

%%%%%%%  Fix various header and footer problems %%%

% Draft mark on the right side of left pages (outside)
% this mark is also the only one visible on single sided.
\newcommand{\draftrmark}{**DRAFT**} 
% Draft mark on the left side of right pages (outside)
\newcommand{\draftlmark}{**DRAFT**} % 

% Macros to make changing the Draft easier
\newcommand{\drmark}[1]{\renewcommand{\draftrmark}{#1}}
\newcommand{\dlmark}[1]{\renewcommand{\draftlmark}{#1}}
\newcommand{\dmark}[1]{\drmark{#1}\dlmark{#1}}

% Format for draft of thesis.  Define our own PageStyle -
% Just like headings, but has foot lines with the date and warning

\if@twoside         % If two-sided printing.
\def\ps@drafthead{
    \let\@mkboth\markboth
    \def\@oddfoot{\rm \today \hfil \sc \draftrmark}
    \def\@evenfoot{\sc \draftlmark \hfil \rm \today }
    \def\@evenhead{\rm \thepage\hfil \sl \leftmark}
    \def\@oddhead{\hbox{}\sl \rightmark \hfil \rm\thepage}
    \def\chaptermark##1{\markboth {\uppercase{\ifnum \c@secnumdepth >\m@ne
        \@chapapp\ \thechapter. \ \fi ##1}}{}}
    \def\sectionmark##1{\markright {\uppercase{\ifnum \c@secnumdepth >\z@
        \thesection. \ \fi ##1}}}
    \pulldownheader                             % Bring header down from edge
%    \pullupfooter                               % Bring footer up
}
\else               % If one-sided printing.
\def\ps@drafthead{
    \let\@mkboth\markboth
    \def\@oddfoot{\rm \today \hfil \sc \draftrmark}
    \def\@oddhead{\hbox{}\sl \rightmark \hfil \rm\thepage}
    \def\chaptermark##1{\markright {\uppercase{\ifnum \c@secnumdepth >\m@ne
        \@chapapp\ \thechapter. \ \fi ##1}}}
    \pulldownheader                             % Bring header down from edge
%    \pullupfooter                               % Bring footer up
}
\fi

% I redefine these formats that were defined in report.sty
% Definition of 'headings' page style 
%  Note the use of ##1 for parameter of \def\chaptermark inside the
%  \def\ps@headings.
%

\if@twoside                                     % If two-sided printing.
\def\ps@headings{\let\@mkboth\markboth
    \def\@oddfoot{}
    \def\@evenfoot{}            % No feet.
    \def\@evenhead{\rm \thepage\hfil \sl \leftmark}     % Left heading.
    \def\@oddhead{\hbox{}\sl \rightmark \hfil \rm\thepage}      % Right heading.
    \def\chaptermark##1{\markboth {\uppercase{\ifnum \c@secnumdepth >\m@ne
        \@chapapp\ \thechapter. \ \fi ##1}}{}}  
    \def\sectionmark##1{\markright {\uppercase{\ifnum \c@secnumdepth >\z@
        \thesection. \ \fi ##1}}}
    \pulldownheader                             % Bring header down from edge
}
\else                                           % If one-sided printing.
\def\ps@headings{\let\@mkboth\markboth
    \def\@oddfoot{}
    \def\@evenfoot{}            %     No feet.
    \def\@oddhead{\hbox {}\sl \rightmark \hfil \rm\thepage}     % Heading.
    \def\chaptermark##1{\markright {\uppercase{\ifnum \c@secnumdepth >\m@ne
        \@chapapp\ \thechapter. \ \fi ##1}}}
    \pulldownheader                             % Bring header down from edge
}
\fi



% You can use the titlepage environment to do it all yourself if you
% don't want to use \maketitle.  If the titlepage environment, the
% paragraph skip is infinitely stretchable, so if you leave a blank line
% between lines that you want space between, the space will stretch so
% that the title page fills up the entire page.

% args: super, reader1, reader2, reader3
\newcommand\supervisoragreement[4]{
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage %%% Supervision Agreement %%%
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{flushright}
   Massachusetts Institute of Technology
\\ Department of \deptname
\\ Cambridge, Massachusetts 02139
\end{flushright}

\underline{\bf Doctoral Thesis Supervision Agreement}

\vspace{.25in}
\begin{tabular}{rl}
   {\small \sc To:}   & Department Graduate Committee
\\ {\small \sc From:} & #1
\end{tabular}

\vspace{.25in}
The program outlined in the proposal:

\vspace{.25in}
\begin{tabular}{rl}
   {\small \sc Title:}  & \title
\\ {\small \sc Author:} & \author
\\ {\small \sc Date:}   & \submissiondate
\end{tabular}

\vspace{.25in}
is adequate for a Doctoral thesis.
I believe that appropriate readers for this thesis would be:

\vspace{.25in}
\begin{tabular}{rl}
   {\small \sc Reader 1:} & #2
\\ {\small \sc Reader 2:} & #3
\\ {\small \sc Reader 3:} & #4
\end{tabular}

\vspace{.25in}
Facilities and support for the research outlined in the proposal are available.
I am willing to supervise the thesis and evaluate the thesis report.

\vspace{.25in}
\begin{tabular}{crc}
  \hspace{2in} & {\sc Signed:} & \\ \cline{3-3}
               &               & {\small \sc \supertitleone} \\
               &               & {\small \sc \supertitletwo} \\
               &               &                             \\
               & {\sc Date:}   & \\ \cline{3-3}
\end{tabular}

\vspace{0in plus 1fill}

Comments: \\
\begin{tabular}{c}
  \hspace{6.25in} \\
  \mbox{} \\ \cline{1-1} \mbox{} \\
  \mbox{} \\ \cline{1-1} \mbox{} \\
  \mbox{} \\ \cline{1-1} \mbox{} \\
%  \mbox{} \\ \cline{1-1} \mbox{} \\
%  \mbox{} \\ \cline{1-1} \mbox{} \\
%  \mbox{} \\ \cline{1-1} \mbox{} \\
\end{tabular}
}

% args: super, reader, readertitle1, readertitle2
\newcommand\readagreement[4]{
  %%% Reader  Agreement %%%
  
\begin{flushright}
   Massachusetts Institute of Technology
\\ Department of \deptname
\\ Cambridge, Massachusetts 02139
\end{flushright}

\underline{\bf Doctoral Thesis Reader Agreement}

\vspace{.25in}
\begin{tabular}{rl}
   {\small \sc To:}   & Department Graduate Committee
\\ {\small \sc From:} & #1
\end{tabular}

\vspace{.25in}
The program outlined in the proposal:

\vspace{.25in}
\begin{tabular}{rl}
   {\small \sc Title:}          & \title
\\ {\small \sc Author:}         & \author
\\ {\small \sc Date:}           & \submissiondate
\\ {\small \sc Supervisor:}     & #1
\\ {\small \sc Other Reader:}   & #2
\end{tabular}

\vspace{.25in}
is adequate for a Doctoral thesis.
I am willing to aid in guiding the research
and in evaluating the thesis report as a reader.

\vspace{.25in}
\begin{tabular}{crc}
  \hspace{2in} & {\sc Signed:} & \\ \cline{3-3}
               &               & {\small \sc #3} \\
               &               & {\small \sc #4} \\
               &               &                                 \\
               & {\sc Date:}   & \\ \cline{3-3}
\end{tabular}

\vspace{0in plus 1fill}

Comments: \\
\begin{tabular}{c}
  \hspace{6.25in} \\
  \mbox{} \\ \cline{1-1} \mbox{} \\
  \mbox{} \\ \cline{1-1} \mbox{} \\
  \mbox{} \\ \cline{1-1} \mbox{} \\
  \mbox{} \\ \cline{1-1} \mbox{} \\
  \mbox{} \\ \cline{1-1} \mbox{} \\
  \mbox{} \\ \cline{1-1} \mbox{} \\
\end{tabular}
\newpage 
}

\def\titlepage{
\begin{center}
{\Large \bf 
   Massachusetts Institute of Technology
\\ Department of \deptname \\}
\vspace{.25in}
{\Large \bf
   Proposal for Thesis Research in Partial Fulfillment
\\ of the Requirements for the Degree of
\\ \degree \\}
\end{center}

\vspace{.5in}

\def\sig{{\small \sc (Signature of Author)}}

\begin{tabular}{rlc}
   {\small \sc Title:}                       & \multicolumn{2}{l}{\title}
\\ {\small \sc Submitted by:}
                            & \author  & \\
                            & \addrone & \\
                            & \addrtwo & \\ \cline{3-3}
			    &	       & \makebox[2in][c]{\sig}
\\ {\small \sc Date of Submission:}          & \multicolumn{2}{l}{\submissiondate}
\\ {\small \sc Expected Date of Completion:} & \multicolumn{2}{l}{\completiondate}
\\ {\small \sc Laboratory:}                  & \multicolumn{2}{l}{\laboratory}
\end{tabular}
}

\def\agreement{
\supervisoragreement{\supervisor}{\readerone}{\readertwo}{\readerthree}
\readagreement{\supervisor}{\readerone}{\readeronetitleone}{\readeronetitletwo}
\readagreement{\supervisor}{\readertwo}{\readertwotitleone}{\readertwotitletwo}
\readagreement{\supervisor}{\readerthree}{\readerthreetitleone}{\readerthreetitletwo}
}

\pagestyle{empty}
